FROM debian:bullseye-slim

LABEL maintainer="Bherly Novrandy <bherly.novrandy@gmail.com>"

ARG GIT_COMMIT
ARG VERSION
ARG APP_NAME
LABEL GIT_COMMIT=$GIT_COMMIT
LABEL VERSION=$VERSION
ARG APP_PATH=/opt/${APP_NAME}
ENV APP_EXECUTABLE=${APP_PATH}/${APP_NAME}

# Because of https://github.com/docker/docker/issues/14914
ENV PATH=$PATH:${APP_PATH}


ENV TZ=Asia/Jakarta

RUN apt-get update && apt-get install -y \
    bash \
    ca-certificates \
    dumb-init \
    openssl \
    tzdata

RUN cp /usr/share/zoneinfo/$TZ /etc/localtime && \
    echo $TZ > /etc/timezone

WORKDIR $APP_PATH

COPY ./bin/$APP_NAME $APP_PATH

RUN chmod +x $APP_EXECUTABLE

# Create appuser
RUN adduser --disabled-password --gecos '' ${APP_NAME}
USER ${APP_NAME}

ENTRYPOINT ["/usr/bin/dumb-init", "--"]

CMD $APP_EXECUTABLE