SHELL                 = /bin/sh

MODULE_NAME			  = gitlab.com/pushm0v/
APP_NAME              = goplate
VERSION               = $(shell git describe --always --tags)
GIT_COMMIT            = $(shell git rev-parse HEAD)
GIT_DIRTY             = $(shell test -n "`git status --porcelain`" && echo "+CHANGES" || true)
BUILD_DATE            = $(shell date '+%Y-%m-%d-%H:%M:%S')

.PHONY: default
default: help

.PHONY: help
help:
	@echo 'Management commands for ${APP_NAME}:'
	@echo
	@echo 'Usage:'
	@echo '    make build                              Compile the project.'
	@echo '    make package                            Build, tag, and push Docker image.'
	@echo '    make deploy                             Deploy to Kubernetes via Helmfile.'
	@echo '    make rollback RELEASE= REVISION=        Rollback via Helm. If REVISION is omitted, it will roll back to the previous release.'
	@echo '    make run ARGS=                          Run with supplied arguments.'
	@echo '    make test                               Run tests on a compiled project.'
	@echo '    make clean                              Clean the directory tree.'
	@echo

.PHONY: build
build:
	@echo "Building ${APP_NAME} ${VERSION}"
	go build -ldflags "-w -X ${MODULE_NAME}${APP_NAME}/version.GitCommit=${GIT_COMMIT}${GIT_DIRTY} -X ${MODULE_NAME}${APP_NAME}/version.Version=${VERSION} -X ${MODULE_NAME}${APP_NAME}/version.Environment=${ENV} -X ${MODULE_NAME}${APP_NAME}/version.BuildDate=${BUILD_DATE}" -o bin/${APP_NAME}
	@echo "===============================\n"

.PHONY: build-package
build-package:
	@echo "Building ${APP_NAME} ${VERSION}"
	GOOS=linux GOARCH=amd64 go build -ldflags "-w -X ${MODULE_NAME}${APP_NAME}/version.GitCommit=${GIT_COMMIT}${GIT_DIRTY} -X ${MODULE_NAME}${APP_NAME}/version.Version=${VERSION} -X ${MODULE_NAME}${APP_NAME}/version.Environment=${ENV} -X ${MODULE_NAME}${APP_NAME}/version.BuildDate=${BUILD_DATE}" -o bin/${APP_NAME}
	@echo "===============================\n"

.PHONY: package
package: build-package
	@echo "Build Docker image ${APP_NAME} ${VERSION} ${GIT_COMMIT}"
	docker build \
		--build-arg APP_NAME=${APP_NAME} \
		--build-arg VERSION=${VERSION} \
		--build-arg GIT_COMMIT=${GIT_COMMIT}${GIT_DIRTY} \
		--tag ${APP_NAME}:${GIT_COMMIT} \
		--tag ${APP_NAME}:${VERSION} \
		--tag ${APP_NAME}:latest \
		.

.PHONY: run
run: build
	@echo "Running ${APP_NAME} ${VERSION}"
	@echo "===============================\n"
	bin/${APP_NAME} ${ARGS}

.PHONY: test
test:
	@echo "Testing ${APP_NAME} ${VERSION}"
	go test -race ./...

.PHONY: clean
clean:
	@echo "Removing ${APP_NAME} ${VERSION}"
	@test ! -e bin/${APP_NAME} || rm bin/${APP_NAME}
