package common

import (
	"bytes"
	"encoding/json"
	"strings"

	"github.com/spf13/viper"
)

type Config struct {
	App App
	Log Log
}

type App struct {
	Host string
	Port int
}

type Log struct {
	Level  string
	Pretty bool
}

func NewConfig() (conf *Config, err error) {
	defaultConfig := Config{}
	defaultConfigJson, err := json.Marshal(&defaultConfig)
	if err != nil {
		return
	}

	viper.SetConfigType("json")
	err = viper.ReadConfig(bytes.NewReader(defaultConfigJson))
	if err != nil {
		return
	}

	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	viper.AllowEmptyEnv(true)
	viper.AutomaticEnv()

	err = viper.Unmarshal(&conf)
	if err != nil {
		return
	}

	return
}
