package server

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"

	"gitlab.com/pushm0v/goplate/app/common"
)

type IServer interface {
	Start()
}

type server struct {
	*common.Option
}

func NewServer(opt *common.Option) IServer {
	return &server{
		Option: opt,
	}
}

func (s *server) Start() {
	var srv http.Server
	idleConnectionClosed := make(chan struct{})
	go func() {
		sigint := make(chan os.Signal, 1)
		signal.Notify(sigint, os.Interrupt)
		<-sigint

		if err := srv.Shutdown(context.Background()); err != nil {

		}
		close(idleConnectionClosed)
	}()

	srv.Addr = fmt.Sprintf("%s:%d", s.Config.App.Host, s.Config.App.Port)

	s.Logger.Info().Msgf("Server is starting at %s", srv.Addr)

	if err := srv.ListenAndServe(); err != http.ErrServerClosed {
		s.Logger.Error().Err(err).Msg("Server failed to start")
	}

	<-idleConnectionClosed
	s.Logger.Info().Msg("Server shutting down")
}
