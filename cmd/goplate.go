package cmd

import (
	"gitlab.com/pushm0v/goplate/app/common"
	"gitlab.com/pushm0v/goplate/app/server"

	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
)

var goplateCmd = &cobra.Command{
	Use:   "goplate",
	Short: "Start Goplate",
	Long:  "Goplate is an example of golang app",
	Run: func(cmd *cobra.Command, args []string) {
		startServer()
	},
}

func startServer() {
	conf, err := common.NewConfig()
	if err != nil {
		log.Error().Msgf("Config error | %v", err)
		panic(err)
	}

	logger := common.NewLogger(conf.Log)

	options := &common.Option{
		Config: conf,
		Logger: logger,
	}

	sv := server.NewServer(options)
	sv.Start()
}
