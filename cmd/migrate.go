package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

var migrateUpCmd = &cobra.Command{
	Use:   "migrate",
	Short: "Migrate Up DB",
	Long:  `Run migration script in ./migrations/sql`,
	Run: func(cmd *cobra.Command, args []string) {
		migrateUp()
	},
}

var migrateDownCmd = &cobra.Command{
	Use:   "migratedown",
	Short: "Migrate Up DB",
	Long:  `Run migration script in ./migrations/sql`,
	Run: func(cmd *cobra.Command, args []string) {
		migrateDown()
	},
}

var migrateNewCmd = &cobra.Command{
	Use:   "migratenew [migration name]",
	Short: "Create a new migration file",
	Long:  `Create a new migration file on folder migrations/sql with timestamp as prefix`,
	Args:  cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		migrateNew()
	},
}

func init() {
	goplateCmd.AddCommand(migrateUpCmd)
	goplateCmd.AddCommand(migrateDownCmd)
	goplateCmd.AddCommand(migrateNewCmd)
}

func migrateUp() {
	fmt.Println("Migrate Up")
}
func migrateDown() {
	fmt.Println("Migrate Down")
}
func migrateNew() {
	fmt.Println("Migrate New")
}
